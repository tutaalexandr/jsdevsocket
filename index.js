var app = require('express')();
var http = require('http').createServer(app);
var path = require('path');
var io = require('socket.io')(http);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public') + '/index.html');
});

io.on('connection', (socket) => {
    console.log('a user connected');

    let users = {};
    let massages = [];
    userInit();
    socket.on('set user name', (name) => {
        setUserData('name', name);
    })

    socket.on('chat message', (msg) => {
        let one = {
            'user' : userThis().name,
            'time' : 1,
            'text': msg
        }
        massages.push(one);

        io.emit('chat message', msg);
    });

    socket.on('disconnect', () => {
        delete users[socket.conn.id];
        console.log(users);
        console.log('user disconnected');
    });доброе

    function setUserData(key, data) {
        let user = socket.conn.id;
        users[user][key] = data;
        console.log(users);
    }

    function userInit() {
        let user = socket.conn.id;
        users[user] = {
            'name' : '',
            'timeConnect' : 0,
        };
        console.log(users);
    }

    function userThis() {
        let user = socket.conn.id;
        return users[user];
    }
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});